class ChatsController < ApplicationController
	# skip_before_action :verify_authenticity_token
  require 'twilio_lib'	
  require 'dialog_flow'

	def index
	end

	def receive_message
		puts "\n\nParams -> #{params.inspect}"

    response = Twilio::TwiML::MessagingResponse.new
    body = params["Body"]

		dialog_flow = DialogFlow.new().query(body)

		puts "\n\nIntent -> #{dialog_flow.intent.display_name}"

		if (dialog_flow.intent.display_name == "Default Welcome Intent")
			query_response = dialog_flow.fulfillment_text
		elsif (dialog_flow.intent.display_name == "Weather")
			location = dialog_flow.parameters.fields["geo-city"].string_value
			date_loc = dialog_flow.parameters.fields["date"].string_value
			period_loc = dialog_flow.parameters.fields["date-period"].string_value

			lat, lon = GeocoderLocationLib.new(location).coordinates

			query_response = ForecastLib.new().weather_by_lat_long(lat, lon)
		end

		puts "\n\nquery_response -> #{query_response.inspect}"

		# query_response = dialog_flow.fulfillment_text
    response.message(body: query_response)
    render xml: response.to_xml
	end

end
