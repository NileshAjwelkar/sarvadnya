Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post '/read-message', to: 'chats#receive_message'

  post '/fulfillment', to: 'webhooks#fulfillments'
end
