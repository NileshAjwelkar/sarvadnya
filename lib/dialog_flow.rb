class DialogFlow
	require "google/cloud/dialogflow"

	attr_accessor :language_code, :session_client, :session

	def initialize
		project_id = "superbot-d9989"
		session_id = "mysession"
		# texts = ["hello"]
		@language_code = "en-US"

		@session_client = Google::Cloud::Dialogflow::Sessions.new
		@session = session_client.class.session_path project_id, session_id
		puts "Session path: #{session}"
	end

	def query(text)
	
		# texts.each do |text|
	  	query_input = { text: { text: text, language_code: language_code } }


	  	response = session_client.detect_intent session, query_input


	  	query_result = response.query_result

	  	# puts "Query Intent:      #{query_result.intent} "
	  	# puts "Query text:        #{query_result.query_text}"
	  	# puts "Intent detected:   #{query_result.intent.display_name}"
	  	# puts "Intent confidence: #{query_result.intent_detection_confidence}"
	  	# puts "Fulfillment text:  #{query_result.fulfillment_text}\n"
		# end

	end
end