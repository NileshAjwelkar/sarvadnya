class ForecastLib
	require 'forecast_io'

	attr_accessor :open_weather, :options

	def initialize()
		ForecastIO.api_key = '822e405d2aee5fdc2701e13d7383847a'
	end

	def weather_by_lat_long(lat, lon)
		# ForecastIO.api_key = '822e405d2aee5fdc2701e13d7383847a'
		weather_resp = ForecastIO.forecast(lat, lon, params: { units: 'si', time: Time.now.to_i })

		weather_forecast = "#{weather_resp.currently.summary} \nTemperature: #{weather_resp.currently.temperature}\nApparent Temperature #{weather_resp.currently.apparentTemperature}\nWind Speed #{weather_resp.currently.windSpeed}"
	end

end