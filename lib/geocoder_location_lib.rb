class GeocoderLocationLib
	attr_accessor :result

	def initialize(search_params)
		@result = Geocoder.search(search_params).first
	end

	def coordinates
		result.coordinates
	end

	def city_name
	end

	def address
		result.address
	end

	def country
		result.country
	end

													
end

