class TwilioLib
	# require 'rubygems'
	# require 'twilio-ruby'

	attr_accessor :client

	def initialize
		# put your own credentials here
		account_sid = 'account_sid'
		auth_token = 'auth_token'

		@client = Twilio::REST::Client.new account_sid, auth_token
	end

	def send_message(msg_string, recipient_number)
		message = client.messages.create(
                             from: sarvadnya_number,
                             body: msg_string,
                             to: "whatsapp:+#{recipient_number}"
                           )
	end

	def whatsapp_call(recipient_number)
		client.api.account.calls.create(
  		from: sarvadnya_number,
  		to: '+#{recipient_number}',
  		url: 'http://example.com'
		)
	end

	def read_message()
	end

	def list_messages
		client.api.account.messages.list
	end

	def sarvadnya_number
		"whatsapp:+14155238886"
	end
end